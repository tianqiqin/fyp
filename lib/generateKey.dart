import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:blockchain_wallet_app/constant.dart';
import 'package:blockchain_wallet_app/main.dart';
import 'package:blockchain_wallet_app/auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GenerateKey extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _GenerateKey();
}

class _GenerateKey extends State<GenerateKey> {
  var isLoading = true;
  var public_key ="";
  var private_key ="";
  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    final response =
    await http.get(Constants().MAIN_URL+Constants().CREATE_ACCOUNT_URL);
    try{
      print(response.body);
      if (response.statusCode == 200) {
        print(response.body);
        setState(() {

          private_key = (json.decode(response.body)['data']['private_key'] as String);
        });
        final finalCreate =
        await http.get(Constants().MAIN_URL+Constants().CREATE_ACCOUNT_URL+"?sk="+(json.decode(response.body)['data']['private_key'] as String));
        try{
          print(finalCreate.body);
          if (finalCreate.statusCode == 200) {
            print(finalCreate.body);

            setState(() {
              isLoading = false;
              public_key = (json.decode(finalCreate.body)['data']['public_key'] as String);
            });
          }
        } catch (e){
          setState(() {
            isLoading = false;
          });
          _fetchData();
          print("tring to recall");
        }
      }
    } catch (e){
      setState(() {
        isLoading = false;
      });
      print("fail");
    }
  }
  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  void Signin() async{
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('private_key', private_key);
    await prefs.setString('public_key', public_key);
    Navigator.pushReplacement(
      context,
      new MaterialPageRoute(builder: (context) => new MyApp()),
    );
  }

  void GoBack(){
    Navigator.pushReplacement(
      context,
      new MaterialPageRoute(builder: (context) => new Auth()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: null,
        body: isLoading?Center(
            child: CircularProgressIndicator()):new Container(
          width: window.physicalSize.width,
          decoration: new BoxDecoration(

            color: Colors.white

          ),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                margin: EdgeInsets.all(20),
                child:Text("Congradulations!",style: new TextStyle(fontSize: 38,fontWeight: FontWeight.bold,color: Colors.cyan),textAlign: TextAlign.center,),
              ),
              new Container(
                margin: EdgeInsets.all(20),
                child:Text("Your private key generated successfully, please keep it and backup.",style: new TextStyle(fontSize: 18,fontWeight: FontWeight.normal),textAlign: TextAlign.center,),
              ),
              new Container(
                margin: EdgeInsets.only(left: 30,right: 30,top: 20),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black26,style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Text("Public Key:\n"+public_key,style: new TextStyle(fontSize: 18,fontWeight:FontWeight.bold,color: Colors.grey),textAlign: TextAlign.left,),
              ),
              new Container(
                margin: EdgeInsets.only(left: 30,right: 30,top: 20,bottom: 20),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26,style: BorderStyle.solid),
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Text("Private Key:\n"+private_key,style: new TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Colors.grey),textAlign: TextAlign.left,),
              ),
              new Container(
                margin: EdgeInsets.all(20),
                child:Text("We will not keep your private key.",style: new TextStyle(fontSize: 18,fontWeight: FontWeight.normal),textAlign: TextAlign.center,),
              ),
              new Container(
                width: window.physicalSize.width-40,
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.only(left: 20,right: 20,top: 5,bottom: 5),
                decoration: BoxDecoration(
                    color: Colors.cyan,
                    borderRadius: new BorderRadius.all(new Radius.circular(4)),
                    boxShadow: [          new BoxShadow (
                      color: Colors.black26,
                      offset: new Offset(0.0, 2.0),
                      blurRadius: 10.0,
                    )]
                ),
                child: new FlatButton(onPressed: ()=>GoBack(), child:
                Text("Use your old account",style: new TextStyle(fontSize: 21,color: Colors.white))),
              ),new Container(
                width: window.physicalSize.width-40,
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.only(left: 20,right: 20,top: 5,bottom: 5),
                decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: new BorderRadius.all(new Radius.circular(4)),
                    boxShadow: [          new BoxShadow (
                      color: Colors.black26,
                      offset: new Offset(0.0, 2.0),
                      blurRadius: 10.0,
                    )]
                ),
                child: new FlatButton(onPressed: ()=>Signin(), child:
                Text("Sign in you account",style: new TextStyle(fontSize: 21,color: Colors.white,))),
              )

            ],
          ),
        )
    );
  }
}