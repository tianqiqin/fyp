import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:blockchain_wallet_app/main.dart';
import 'package:blockchain_wallet_app/generateKey.dart';
import 'package:blockchain_wallet_app/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(new MaterialApp(
    home: Auth(),
  ));
}
class Auth extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _AuthD();
}

class _AuthD extends State<Auth> {
  String skText = "5KSXyTMEoFwUHzWxqSewtrqT18v3a9AUMSzocsU892gbB5SPaF4d6007ff69e62a838954a14b2171f22b189c73b55199689eb4c5ee68a022c1921";
  bool isAllow = false;
  var isLoading = false;

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    final response =
    await http.get(Constants().MAIN_URL+Constants().CREATE_ACCOUNT_URL+"?sk="+skText);
    try{
      print(response.body);
      if (response.statusCode == 200) {
        print(response.body);
        if (json.decode(response.body)['status'] as bool){
          return;
        }
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('private_key', skText);
        await prefs.setString('public_key', (json.decode(response.body)['data']['public_key'] as String));
        setState(() {
          isLoading = false;
        });
        Navigator.pushReplacement(
          context,
          new MaterialPageRoute(builder: (context) => new MyApp()),
        );

      }
    } catch (e){
      setState(() {
        isLoading = false;
      });
      print("fail");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: null,
        body: new Container(
          width: window.physicalSize.width,
          decoration: new BoxDecoration(

            image: DecorationImage(
              image: AssetImage('assets/images/bg10.jpg'),
              fit: BoxFit.cover,
            ),

          ),
          child: viewMain(),
        )
    );
  }

  void GenerateKeyView(){
    Navigator.pushReplacement(
      context,
      new MaterialPageRoute(builder: (context) => new GenerateKey()),
    );
  }


  viewMain(){
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,

      children: <Widget>[
        new Container(
            width: 200,
            height: 200,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(bottom: 100),
            decoration: new BoxDecoration(

              image: DecorationImage(
                image: AssetImage('assets/images/logo.png'),
                fit: BoxFit.cover,
              ),

            )
        ),new Column(
            children: <Widget>[
              new Container(
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.only(left: 25,right: 25,top: 5,bottom: 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.all(new Radius.circular(4)),
                    boxShadow: [          new BoxShadow (
                      color: Colors.black26,
                      offset: new Offset(0.0, 2.0),
                      blurRadius: 4.0,
                    )]
                ),
                child: TextField(
                  style: new TextStyle(fontSize: 21),

                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Please enter your secret key',
                  ),
                  onChanged: (text) {
                    setState(() {
                      skText = text;
                    });
                  },
                ),
              ),
              new Container(
                width: window.physicalSize.width-40,
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.only(left: 20,right: 20,top: 5,bottom: 5),
                decoration: BoxDecoration(
                  color: Colors.cyan,
                  borderRadius: new BorderRadius.all(new Radius.circular(4)),
                  boxShadow: [          new BoxShadow (
                    color: Colors.black26,
                    offset: new Offset(0.0, 2.0),
                    blurRadius: 4.0,
                  )]
                ),
                child: new FlatButton(onPressed: ()=>_fetchData(), child:
                Text("Sign in your wallet account",style: new TextStyle(fontSize: 21,color: Colors.white))),
              ),
              new Container(
                width: window.physicalSize.width-40,
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.only(left: 20,right: 20,top: 5,bottom: 5),
                decoration: BoxDecoration(
                    color: Colors.redAccent,
                    borderRadius: new BorderRadius.all(new Radius.circular(4)),
                    boxShadow: [          new BoxShadow (
                      color: Colors.black26,
                      offset: new Offset(0.0, 2.0),
                      blurRadius: 4.0,
                    )]
                ),
                child: new FlatButton(onPressed: ()=>GenerateKeyView(), child:
                Text("Generate new account",style: new TextStyle(fontSize: 21,color: Colors.white))),
              )
            ],

        )
      ],
    );
  }
}