import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:io';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';
import 'package:blockchain_wallet_app/auth.dart';
import 'package:blockchain_wallet_app/withDraw.dart';
import 'package:blockchain_wallet_app/bankCard.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Profile extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _ProfileS();
}

class _ProfileS extends State<Profile> {

  String publicKey = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _contentWidget(),
    );
  }
  @override
  void initState() {
    super.initState();
    getData();
  }
  Future getData() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      publicKey = prefs.getString('public_key');
    });
  }
  void logOut(){
    Navigator.pushReplacement(
      context,
      new MaterialPageRoute(builder: (context) => new Auth()),
    );

  }
  void goToBankCard(){
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new BankCard()),
    );

  }
  void goToWithdraw(){
    Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new WithDraw()),
    );

  }

  _contentWidget() {
    final bodyHeight = MediaQuery.of(context).size.height - MediaQuery.of(context).viewInsets.bottom;
    return  Container(
      color: const Color(0xFFFFFFFF),
      child:  Column(
        children: <Widget>[

          Padding(
              padding: EdgeInsets.all(10),
              child:Text("Account: "+publicKey,style: new TextStyle(fontSize: 21,fontWeight: FontWeight.normal),)),
          Padding(
            padding: EdgeInsets.symmetric( vertical: 10.0),
            child: new Container(
              width: window.physicalSize.width-40,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.only(top: 5,bottom: 5),
              decoration: BoxDecoration(
                  color: Colors.cyan,
                  borderRadius: new BorderRadius.all(new Radius.circular(4)),
                  boxShadow: [          new BoxShadow (
                    color: Colors.black26,
                    offset: new Offset(0.0, 2.0),
                    blurRadius: 4.0,
                  )]
              ),
              child: new FlatButton(onPressed: ()=>goToBankCard(), child:
              Text("Top Up Your Account",style: new TextStyle(fontSize: 21,color: Colors.white))),
            ),
          ),Padding(
            padding: EdgeInsets.symmetric( vertical: 10.0),
            child: new Container(
              width: window.physicalSize.width-40,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.only(top: 5,bottom: 5),
              decoration: BoxDecoration(
                  color: Colors.indigo,
                  borderRadius: new BorderRadius.all(new Radius.circular(4)),
                  boxShadow: [          new BoxShadow (
                    color: Colors.black26,
                    offset: new Offset(0.0, 2.0),
                    blurRadius: 4.0,
                  )]
              ),
              child: new FlatButton(onPressed: ()=>goToWithdraw(), child:
              Text("Withdraw Your Money",style: new TextStyle(fontSize: 21,color: Colors.white))),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric( vertical: 10.0),
            child: new Container(
              width: window.physicalSize.width-40,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.only(top: 5,bottom: 5),
              decoration: BoxDecoration(
                  color: Colors.orange,
                  borderRadius: new BorderRadius.all(new Radius.circular(4)),
                  boxShadow: [          new BoxShadow (
                    color: Colors.black,
                    offset: new Offset(0.0, 2.0),
                    blurRadius: 4.0,
                  )]
              ),
              child: new FlatButton(onPressed: ()=>logOut(), child:
              Text("Log out account",style: new TextStyle(fontSize: 21,color: Colors.white))),
            ),
          )
        ],
      ),
    );
  }
}