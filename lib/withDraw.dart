import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:io';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';
import 'package:blockchain_wallet_app/auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WithDraw extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _WithDraw();
}

class _WithDraw extends State<WithDraw> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _contentWidget(),
    );
  }
  @override
  void initState() {
    super.initState();
  }



  _contentWidget() {
    final bodyHeight = MediaQuery.of(context).size.height - MediaQuery.of(context).viewInsets.bottom;
    return  Container(
      color: const Color(0xFFFFFFFF),
      child:  Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 34),
            child: null,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
            child: Text("Your Amount",style: new TextStyle(fontSize: 21,color: Colors.black),),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
            child: Text("\$0",style: new TextStyle(fontSize: 26,color: Colors.black),),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
            child: TextField(
              style: new TextStyle(fontSize: 21,color: Colors.black),
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: '0000 0000 0000 0000',
              ),
              onChanged: (text) {

              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
            child: TextField(
              style: new TextStyle(fontSize: 21,color: Colors.black),
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Amount. Eg. 0',
              ),
              onChanged: (text) {

              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
            child: TextField(
              style: new TextStyle(fontSize: 21,color: Colors.black),
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Your code: Eg. 000',
              ),
              onChanged: (text) {

              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric( vertical: 10.0),
            child: new Container(
              width: window.physicalSize.width-40,
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.only(top: 5,bottom: 5),
              decoration: BoxDecoration(
                  color: Colors.cyan,
                  borderRadius: new BorderRadius.all(new Radius.circular(4)),
                  boxShadow: [          new BoxShadow (
                    color: Colors.black26,
                    offset: new Offset(0.0, 2.0),
                    blurRadius: 4.0,
                  )]
              ),
              child: new FlatButton(onPressed: ()=>null, child:
              Text("WithDraw",style: new TextStyle(fontSize: 21,color: Colors.white))),
            ),
          )
        ],
      ),
    );
  }
}