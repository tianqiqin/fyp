import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter/services.dart';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:io';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';
import 'package:blockchain_wallet_app/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
class Pay extends StatefulWidget{
  @override
  _PayS createState() => new _PayS();
}

class _PayS extends State<Pay>{
  String barcode = "";
  bool isPaying = false;
  GlobalKey globalKey = new GlobalKey();
  String skText = "";
  String _dataString = "";
  String money = "0";
  final TextEditingController _textController =  TextEditingController();

  @override
  initState() {
    super.initState();
    getData();

  }

  Future getData() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      skText = prefs.getString('public_key');
      _dataString = new Constants().MAIN_URL+"transactions/new?rt="+skText+"&&price="+money+"&&sr=";
    });

    print(_dataString);
  }

  @override
  Widget build(BuildContext context) {
    final bodyHeight = MediaQuery.of(context).size.height - MediaQuery.of(context).viewInsets.bottom;
    print(bodyHeight);
    return Scaffold(

        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
                  child: Text("Transfer amount", style: new TextStyle(fontSize: 25,color: Colors.black))),
                Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
                child: TextField(
                  style: new TextStyle(fontSize: 21,color: Colors.black),
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Amount. Eg. 0',
                  ),
                  onChanged: (text) {
                    setState(() {
                      money = text;
                    });
                    getData();
                  },
                ),
              ),
              Expanded(
                child:  Center(
                  child: RepaintBoundary(
                    key: globalKey,
                    child: QrImage(
                      version: 10,
                      data: _dataString,
                      size: 0.5 * bodyHeight,
                        gapless: true,
                      onError: (ex) {
                        print("[QR] ERROR - $ex");
                        AlertDialog(
                          title: Text("Error"),
                          content: Text("Error! Maybe your input value is too long?"),
                        );
                      },
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric( vertical: 10.0),
                child: new Container(
                  width: window.physicalSize.width-40,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.only(top: 5,bottom: 5),
                  decoration: BoxDecoration(
                      color: Colors.cyan,
                      borderRadius: new BorderRadius.all(new Radius.circular(4)),
                      boxShadow: [          new BoxShadow (
                        color: Colors.black26,
                        offset: new Offset(0.0, 2.0),
                        blurRadius: 4.0,
                      )]
                  ),
                  child: new FlatButton(onPressed: ()=>scan(), child:
                  Text("Scan qrcode",style: new TextStyle(fontSize: 21,color: Colors.white))),
                ),
              )
              ,
            ],
          ),
        ));
  }

  _pay(String url) async {
    setState(() {
      isPaying = true;
    });
    final response =
    await http.get(url);
    try{
      print(response.body);
      if (response.statusCode == 200) {
        print(response.body);
        if (json.decode(response.body)['status'] as bool){
          return;
        }

        setState(() {
          isPaying = false;
        });
        Navigator.of(context).pop();

      }
    } catch (e){
      setState(() {
        isPaying = false;
      });
      print("fail");
    }
  }

  Future scan() async {

    try {
      final prefs = await SharedPreferences.getInstance();
      // scan success
      String barcode = await BarcodeScanner.scan();
      barcode = barcode + prefs.getString('private_key');
      var uri = Uri.parse(await barcode);
      String receipt =await uri.queryParameters['rt'];
      String price =await uri.queryParameters['price'];

      await showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text('Confirm!'),
            content: new Text('You will pay \$'+price +' to\n'+receipt),
            actions: <Widget>[
              FlatButton(
                child: Text('Confirm'),
                onPressed: () {
                  _pay(barcode);
                },
              ),FlatButton(
                child: Text('Cancel'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },

      );
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException{
      setState(() => this.barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }
}