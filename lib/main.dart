import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:blockchain_wallet_app/pay.dart';
import 'package:blockchain_wallet_app/auth.dart';
import 'package:blockchain_wallet_app/profile.dart';
import 'package:blockchain_wallet_app/constant.dart';
import 'components/historyItem.dart';
import 'package:shared_preferences/shared_preferences.dart';

// A ListItem that contains data to display a message
class MessageItem {
  final String sender;
  final String recipient;
  final double time;
  final double amount;

  MessageItem({this.sender, this.recipient,this.time,this.amount});
  factory MessageItem.fromJson(Map<String, dynamic> json) {
    return new MessageItem(
        sender: json['sender'],
        recipient: json['recipient'],
        time: (json['timestamp'] as num).toDouble(),
        amount: (json['amount'] as num).toDouble()
    );
  }
}


class MyApp extends StatefulWidget {
  @override
  _MyAppD createState() => _MyAppD();
}
class _MyAppD extends State<MyApp> {
  String title = "WALLET";
  int _currentIndex = 0;
  String eCardNumber = "";
  String pk = "";
  String urlMain = new Constants().MAIN_URL;
  var historyList = [];
  var isLoading = false;
  var isNetworking = true;
  double todayPaid = 0;
  double accountAmount = 0;

  _fetchData(String url,String pk) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      isLoading = true;
      eCardNumber = prefs.getString('public_key');
      pk = prefs.getString('private_key');

    });
    final response =
    await http.get(url+"chains/account?sk="+pk);
    try{
      if (response.statusCode == 200) {
        historyList = (json.decode(response.body)['data']['history'] as List)
            .map((data) => new MessageItem.fromJson(data))
            .toList();
        print(json.decode(response.body)['data']['amount'].toString());
        setState(() {
          isLoading = false;
          isNetworking = true;
          accountAmount = double.parse(json.decode(response.body)['data']['amount'].toString());

        });
        print(accountAmount);
      }
    } catch (e){
      print(e);
      setState(() {
        isNetworking = false;
      });
    }
  }
  @override
  void initState() {
    super.initState();
    _fetchData(urlMain, pk);
  }

  @override
  Widget build(BuildContext context) {

    /**
     * Display transctions history
     * by list
     */
    Container historyItemColumn(String sender,String recipient, double amount, double time){
        return new Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(top: 10),
          decoration: new BoxDecoration(

            color: sender == eCardNumber?Colors.redAccent.shade700:Colors.lightBlue.shade400,
            borderRadius: new BorderRadius.all(new Radius.circular(8.0)),
            boxShadow: [new BoxShadow (
              color: const Color.fromRGBO(200,200,200,0.6),
              offset: new Offset(0, 1.0),
            )],

          ),
          child: sender == eCardNumber?Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
                Text("\$ "+ amount.toString(),style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600,color: Colors.white),),
                Text("Paid to: " + recipient.toString(),style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400,color: Colors.white)),
                Text(new DateTime.fromMillisecondsSinceEpoch(time.toInt()*1000).toLocal().toString(),style: TextStyle(fontSize: 14,fontWeight: FontWeight.w600,color: Colors.white)),
            ],
          ):Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("\$ "+ amount.toString(),style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600,color: Colors.white),),
              Text("Recieved from: " + sender.toString(),style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400,color: Colors.white)),
              Text(new DateTime.fromMillisecondsSinceEpoch(time.toInt()*1000).toLocal().toString(),style: TextStyle(fontSize: 14,fontWeight: FontWeight.w600,color: Colors.white)),
            ],
          ),
        );
    };

    Future<Null> _handleRefresh() async {
      await new Future.delayed(new Duration(seconds: 3));
      _fetchData(urlMain, pk);
      return null;
    }

    Widget transferHistory =
    new RefreshIndicator(

        child: ListView.builder(
          // Let the ListView know how many items it needs to build
          shrinkWrap: true,
          itemCount: historyList.length,
          // Provide a builder function. This is where the magic happens! We'll
          // convert each item into a Widget based on the type of item it is.
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            final item = historyList[index];
              if (item is MessageItem) {
                return historyItemColumn(item.sender,item.recipient,item.amount,item.time);
              }
            },
        ),
      onRefresh:_handleRefresh,
    );

    Widget transferHistoryHeading = new Container(
      margin: EdgeInsets.only(top:0),
      width: window.physicalSize.width,
      height: window.physicalSize.height - 260,
      padding: EdgeInsets.all(10),
      decoration: new BoxDecoration(

        color: Colors.white
      ),
      child: new Column(
        children: <Widget>[
          isNetworking?
          (isLoading ?
          Container(
            height: 260,
            child: Center(
                child: CircularProgressIndicator()),
          )
              : transferHistory):
          Container(
            height: 260,
            child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Network error",style: new TextStyle(fontWeight: FontWeight.w600,fontSize: 20,color: Colors.white)),
                    RaisedButton(
                        onPressed: _handleRefresh,
                        child: Text("Reload",style: new TextStyle(fontWeight: FontWeight.w600,fontSize: 16,color: Colors.white))
                    )
                  ],
                ),
            ),
          ),
        ],
      ),
    );

    /**
     * Blockchain e-card display
     * a group widget
     */
    Widget blockChainCardPanel = new Container(
        height: 200,
        padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.only(left: 0,right: 0,top: 0,bottom: 0),
        width: window.physicalSize.width,
        decoration: new BoxDecoration(

          image: DecorationImage(
            image: AssetImage('assets/images/bg1.jpg'),
            fit: BoxFit.cover,
          ),
          borderRadius: new BorderRadius.all(new Radius.circular(8.0)),
          boxShadow: [new BoxShadow (
            color: const Color.fromRGBO(134,134,134,0.6),
            offset: new Offset(1, 1.0),
          )],

        ),
        child: new Column(

          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.end,

          children: [
            new Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.end,

                    children: [
                      new Text(
                        "Amount",
                        style: new TextStyle(
                          fontSize: 22.0,
                          fontWeight: FontWeight.w900,
                          color: Colors.white70,
                        ),
                      ),
                      new Text(
                        "\$ "+accountAmount.toString(),
                        style: new TextStyle(
                          fontSize: 32.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,

                        ),
                      )
                    ],
                  ),
                )

              ],
            ),

            new Text("ADDRESS:"+eCardNumber,style: new TextStyle(
              fontSize: 12.0,
              fontWeight: FontWeight.w600,
              color: Colors.white,
            )),
          ],
        )
    );

    Widget HomePage = new Container(
      width: window.physicalSize.width,
      height: window.physicalSize.height,
      margin: EdgeInsets.only(left: 10,right: 10,top: 0,bottom: 0),

      child: new Column(
        children: <Widget>[blockChainCardPanel,transferHistory],
      ),
    );
      /*
     * Main app body,
     * Contain all widgets
     */
    final List<Widget> _children = [
      HomePage,Pay(),
    new Profile()];
    void onTabTapped(int index) {
      switch (index){
        case 0:
        setState(() {
          title = "WALLET";
        });
        _fetchData(urlMain, pk);
        break;
        case 1:
        setState(() {
          title = "QrCode";
        });
        break;
        case 2:
        setState(() {
          title = "Profile";
        });
        break;
        default:
          setState(() {
            title = "WALLET";
          });
          break;

      }
      setState(() {
        _currentIndex = index;
      });
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(title,style: new TextStyle(color: Colors.black87),),
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body:_children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex, // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.account_balance_wallet ),
            title: new Text('Wallet'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.center_focus_strong),
            title: new Text('QrCode'),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile')
          )
        ],
      ),
    );
  }

}

